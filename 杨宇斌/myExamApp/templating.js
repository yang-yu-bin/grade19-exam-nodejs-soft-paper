let nunjucks=require('nunjucks')


function createEnv(path,opts){
    path=path||'views';
    opts=opts||{};
    let getOpts={
        autoercape:opts.autoercape===undefined?true:opts.autoercape,
        watch:opts.watch===undefined?true:opts.watch,
        noCache:opts.noCache===undefined?true:opts.noCache,
    }
    let env=nunjucks.configure(path,getOpts);
    return env;
}
module.exports=async(ctx,next)=>{
    ctx.render=function(view,model){
        let env=createEnv('views',{autoercape:false});
        ctx.body=env.render(view,model)
    }
    await next();
}